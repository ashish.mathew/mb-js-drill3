function change_id(prop,val)
{
    if (prop === "name")
    {
        return "clark"    
    }
    else if(prop === "age")
    {
        return val + 10;
    }
    else{
        return "Downtown"
    }
}
function obj_map(obj,change)
{
    if(typeof(obj) != "object" || typeof(change) != "function")
    {
        throw Error("invalid argument");
    }
    let new_obj = {};
    for (let i in obj)
    {

        new_obj[i] = change(i,obj[i]);
    }
    return new_obj;
}
export {obj_map,change_id};
