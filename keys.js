function display_keys(obj)
{
    if (typeof(obj) != "object")
    {
        throw Error("invalid argument");
    }
    return Object.keys(obj)
}
export {display_keys}