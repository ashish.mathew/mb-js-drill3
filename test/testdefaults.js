import { add_defaults } from "../defaults.js";

try {
    var iceCream = {flavor: "chocolate"};
    var defaults = {flavor: "vanilla", sprinkles: "lots"};
    console.log(add_defaults(iceCream,defaults));
} catch (error) {
    console.log(error);
}