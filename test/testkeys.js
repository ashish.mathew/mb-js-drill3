import {display_keys} from "../keys.js";

try {
    const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
    console.log(display_keys(testObject));   
} catch (error) {
    console.log(error);
}