import { display_values } from "../values.js";

try {
    const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
    console.log(display_values(testObject));
} catch (error) {
    console.log(error);
}