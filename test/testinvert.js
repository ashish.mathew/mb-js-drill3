import { invert } from "../invert.js";

try {
    const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
    console.log(invert(testObject));
} catch (error) {
    console.log(error);
}