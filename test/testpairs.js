import { display_pairs } from "../pairs.js";

try {
    const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
    console.log(display_pairs(testObject));
} catch (error) {
    console.log(error);
}