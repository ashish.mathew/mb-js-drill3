function display_values(obj)
{
    if(typeof(obj) != "object")
    {
        throw Error("invalid argument");
    }
    return Object.values(obj);
}
export {display_values};