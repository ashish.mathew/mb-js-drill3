function display_pairs(obj)
{
    if(typeof(obj) != "object")
    {
        throw Error("invalid arguments");
    }
    let arr_pairs = [];
    for (let i in obj)
    {
        arr_pairs.push(Array(i,obj[i]))
    }
    return arr_pairs;
}
export {display_pairs};