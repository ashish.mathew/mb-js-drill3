function invert(obj)
{
    if(typeof(obj) != "object")
    {
        throw Error("invalid argument passed");
    }
    let new_obj = {};
    for (let i in obj)
    {
        new_obj[obj[i]] = i;
    }
    return new_obj;
}
export {invert};