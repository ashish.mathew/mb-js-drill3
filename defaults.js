function add_defaults(obj,defaultprops)
{
    if(typeof(obj) != "object" || typeof(defaultprops) != "object")
    {
        throw Error("invalid arguments");
    }
    let obj_props = Object.keys(obj);
    let def_props = Object.keys(defaultprops);
    let diff = def_props.filter(props => !obj_props.includes(props))
    for (let i in diff)
    {
        obj[diff[i]] = defaultprops[diff[i]];
    }
    return obj;
}

export {add_defaults};